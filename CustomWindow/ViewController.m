//
//  ViewController.m
//  CustomWindow
//
//  Created by maulyn on 2018/5/28.
//  Copyright © 2018年 TGCASCE. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)awakeFromNib {
    
    [super awakeFromNib];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (void)viewWillAppear {
    
    [super viewWillAppear];
    CGFloat windowWidth = self.view.bounds.size.width;
    CGFloat windowHeight = self.view.bounds.size.height;
    [self.view.window setRestorable:NO];
    [self.view.window setFrame:NSMakeRect(self.view.window.frame.origin.x, self.view.window.frame.origin.y, windowWidth, windowHeight) display:NO];
    self.view.window.contentView.frame = NSMakeRect(0, 0, windowWidth, windowHeight);
}

@end
