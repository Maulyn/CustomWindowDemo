//
//  SWSTCustomWindowButtonView.m
//  SWST_MAC
//
//  Created by maulyn on 2017/9/23.
//  Copyright © 2017年 cvte. All rights reserved.
//

#import "SWSTCustomWindowButtonView.h"

@interface SWSTCustomWindowButtonView ()

@property (nonatomic, assign) BOOL mouseInside;
@property (nonatomic, strong, readwrite) NSButton *closeButton;
@property (nonatomic, strong, readwrite) NSButton *miniButton;
@property (nonatomic, strong, readwrite) NSButton *maxButton;

@end

@implementation SWSTCustomWindowButtonView

- (instancetype)initWithFrame:(NSRect)frameRect {
    
    self = [super initWithFrame:frameRect];
    if (self) {
        [self commonInitialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)decoder {
    
    self = [super initWithCoder:decoder];
    if (self) {
        [self commonInitialize];
    }
    return self;
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateTrackingAreas {
    
    [super updateTrackingAreas];
    NSTrackingArea *const trackingArea = [[NSTrackingArea alloc] initWithRect:NSZeroRect options:(NSTrackingMouseEnteredAndExited | NSTrackingActiveAlways | NSTrackingInVisibleRect) owner:self userInfo:nil];
    [self addTrackingArea:trackingArea];
}

- (void)mouseEntered:(NSEvent *)event {
    
    [super mouseEntered:event];
    self.mouseInside = YES;
    [self setNeedsDisplayForStandardWindowButtons];
}

- (void)mouseExited:(NSEvent *)event {
    
    [super mouseExited:event];
    self.mouseInside = NO;
    [self setNeedsDisplayForStandardWindowButtons];
}

- (BOOL)_mouseInGroup:(NSButton *)button {
    
    return self.mouseInside;
}

#pragma mark - Public Methods

- (void)resetButtonPosition {
    
    if (self.maxButton.hidden) {
        if (self.miniButton.hidden) {
            
        } else {
            if (self.closeButton.hidden) {
                self.miniButton.frame = NSMakeRect(10, 0, self.miniButton.bounds.size.width, self.miniButton.bounds.size.height);
            } else {
                self.miniButton.frame = NSMakeRect(30, 0, self.miniButton.bounds.size.width, self.miniButton.bounds.size.height);
            }
        }
    } else {
        if (self.miniButton.hidden) {
            if (self.closeButton.hidden) {
                self.maxButton.frame = NSMakeRect(10, 0, self.maxButton.bounds.size.width, self.maxButton.bounds.size.height);
            } else {
                self.maxButton.frame = NSMakeRect(30, 0, self.maxButton.bounds.size.width, self.maxButton.bounds.size.height);
            }
        } else {
            if (self.closeButton.hidden) {
                self.miniButton.frame = NSMakeRect(10, 0, self.miniButton.bounds.size.width, self.miniButton.bounds.size.height);
                self.maxButton.frame = NSMakeRect(30, 0, self.maxButton.bounds.size.width, self.maxButton.bounds.size.height);
            } else {
                self.closeButton.frame = NSMakeRect(10, 0, self.closeButton.bounds.size.width, self.closeButton.bounds.size.height);
                self.miniButton.frame = NSMakeRect(30, 0, self.miniButton.bounds.size.width, self.miniButton.bounds.size.height);
                self.maxButton.frame = NSMakeRect(50, 0, self.maxButton.bounds.size.width, self.maxButton.bounds.size.height);
            }
        }
    }
}

#pragma mark - Private Methods

- (void)commonInitialize {
    
    self.closeButton = [NSWindow standardWindowButton:NSWindowCloseButton forStyleMask:3];
    self.closeButton.frame = NSMakeRect(10, 0, self.closeButton.bounds.size.width, self.closeButton.bounds.size.height);
    
    self.miniButton = [NSWindow standardWindowButton:NSWindowMiniaturizeButton forStyleMask:3];
    self.miniButton.frame = NSMakeRect(30, 0, self.miniButton.bounds.size.width, self.miniButton.bounds.size.height);
    
    self.maxButton = [NSWindow standardWindowButton:NSWindowZoomButton forStyleMask:3];
    self.maxButton.frame = NSMakeRect(50, 0, self.maxButton.bounds.size.width, self.maxButton.bounds.size.height);
    
    [self addSubview:self.closeButton];
    [self addSubview:self.miniButton];
    [self addSubview:self.maxButton];
}

- (void)addNotifications {
    
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self selector:@selector(applicationWillBecomeActive:) name:NSApplicationWillBecomeActiveNotification object:NSApp];
    [defaultCenter addObserver:self selector:@selector(applicationDidResignActive:) name:NSApplicationDidResignActiveNotification object:NSApp];
    [defaultCenter addObserver:self selector:@selector(windowActiveChanged:) name:NSWindowDidBecomeMainNotification object:nil];
}

#pragma mark - Private Methods

- (void)setNeedsDisplayForStandardWindowButtons {
    
    [self.closeButton setNeedsDisplay];
    [self.miniButton setNeedsDisplay];
    [self.maxButton setNeedsDisplay];
}

#pragma mark - Notifications

- (void)applicationWillBecomeActive:(NSNotification *)notification {
    
    self.mouseInside = YES;
    [self setNeedsDisplayForStandardWindowButtons];
    
    self.mouseInside = NO;
    [self setNeedsDisplayForStandardWindowButtons];
}

- (void)applicationDidResignActive:(NSNotification *)notification {
    
    self.mouseInside = NO;
    [self setNeedsDisplayForStandardWindowButtons];
}

- (void)windowActiveChanged:(NSNotification *)notification {
    
    self.mouseInside = NO;
    [self setNeedsDisplayForStandardWindowButtons];
}

@end
