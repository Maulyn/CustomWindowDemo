//
//  SWSTMovableBackgroundView.h
//  TestMacApp
//
//  Created by maulyn on 2017/9/18.
//  Copyright © 2017年 maulyn. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SWSTMovableBackgroundView : NSView

@property (nonatomic, assign) CGFloat mouseDragDetectionThreshold;

@end
