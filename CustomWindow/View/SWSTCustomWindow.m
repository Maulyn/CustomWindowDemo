//
//  SWSTCustomWindow.m
//  SWST_MAC
//
//  Created by maulyn on 2017/12/6.
//  Copyright © 2017年 cvte. All rights reserved.
//

#import "SWSTCustomWindow.h"

@implementation SWSTCustomWindow

- (void)awakeFromNib {
    
    [super awakeFromNib];
    NSArray *subviews = self.contentView.superview.subviews;
    for (NSView *view in subviews) {
        if ([view isKindOfClass:NSClassFromString(@"NSTitlebarContainerView")]) {
            [view removeFromSuperview];
        }
    }
}

- (BOOL)canBecomeKeyWindow {
    
    return YES;
}

- (BOOL)canBecomeMainWindow {
    
    return YES;
}

@end
