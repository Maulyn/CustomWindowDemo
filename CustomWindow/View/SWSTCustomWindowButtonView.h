//
//  SWSTCustomWindowButtonView.h
//  SWST_MAC
//
//  Created by maulyn on 2017/9/23.
//  Copyright © 2017年 cvte. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SWSTCustomWindowButtonView : NSView

@property (nonatomic, strong, readonly) NSButton *closeButton;
@property (nonatomic, strong, readonly) NSButton *miniButton;
@property (nonatomic, strong, readonly) NSButton *maxButton;

- (void)resetButtonPosition;

@end
