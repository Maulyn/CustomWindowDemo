//
//  SWSTCustomToolBar.h
//  SWST_MAC
//
//  Created by maulyn on 2017/9/23.
//  Copyright © 2017年 cvte. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface SWSTCustomToolBar : NSView

@property (nonatomic, assign) BOOL canClose;
@property (nonatomic, assign) BOOL canMiniaturize;
@property (nonatomic, assign) BOOL canZoom;
@property (nonatomic, assign) BOOL canMove;

@property (nonatomic, assign) NSRect customButtonFrame;

- (void)newTarget:(id)target forCloseButton:(SEL)action;

@end
