//
//  SWSTCustomToolBar.m
//  SWST_MAC
//
//  Created by maulyn on 2017/9/23.
//  Copyright © 2017年 cvte. All rights reserved.
//

#import "SWSTCustomToolBar.h"
#import "SWSTMovableBackgroundView.h"
#import "SWSTCustomWindowButtonView.h"

@interface SWSTCustomToolBar ()

@property (nonatomic, strong) SWSTMovableBackgroundView *movableView;
@property (nonatomic, strong) SWSTCustomWindowButtonView *buttonView;

@end

@implementation SWSTCustomToolBar

- (instancetype)initWithFrame:(NSRect)frameRect {
    
    self = [super initWithFrame:frameRect];
    if (self) {
        [self commonInitialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)decoder {
    
    self = [super initWithCoder:decoder];
    if (self) {
        [self commonInitialize];
    }
    return self;
}

#pragma mark - Public Methods

- (void)newTarget:(id)target forCloseButton:(SEL)action {
    
    [self.buttonView.closeButton setTarget:target];
    [self.buttonView.closeButton setAction:action];
}

#pragma mark - Private Methods

- (void)commonInitialize {
    
    self.canClose = YES;
    self.canMiniaturize = YES;
    self.canZoom = YES;
    self.canMove = YES;
    self.movableView = [[SWSTMovableBackgroundView alloc] initWithFrame:self.bounds];
    self.movableView.autoresizingMask = NSViewWidthSizable | NSViewHeightSizable;
    self.buttonView = [[SWSTCustomWindowButtonView alloc] initWithFrame:NSMakeRect(0, 0, 74, 16)];
    self.customButtonFrame = self.buttonView.frame;
    
    [self addSubview:self.movableView];
    [self addSubview:self.buttonView];
}

#pragma mark - Setter

- (void)setCanClose:(BOOL)canClose {
    
    if (_canClose == canClose) {
        return;
    }
    _canClose = canClose;
    self.buttonView.closeButton.hidden = !canClose;
    [self.buttonView resetButtonPosition];
}

- (void)setCanMiniaturize:(BOOL)canMiniaturize {
    
    if (_canMiniaturize == canMiniaturize) {
        return;
    }
    _canMiniaturize = canMiniaturize;
    self.buttonView.miniButton.hidden = !canMiniaturize;
    [self.buttonView resetButtonPosition];
}

- (void)setCanZoom:(BOOL)canZoom {
    
    if (_canZoom == canZoom) {
        return;
    }
    _canZoom = canZoom;
    self.buttonView.maxButton.hidden = !canZoom;
    [self.buttonView resetButtonPosition];
}

- (void)setCanMove:(BOOL)canMove {
    
    if (_canMove == canMove) {
        return;
    }
    _canMove = canMove;
    self.movableView.hidden = !canMove;
}

- (void)setCustomButtonFrame:(NSRect)customButtonFrame {
    
    if (NSEqualRects(_customButtonFrame, customButtonFrame)) {
        return;
    }
    _customButtonFrame = customButtonFrame;
    self.buttonView.frame = customButtonFrame;
}

@end
