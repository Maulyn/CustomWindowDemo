//
//  AppDelegate.m
//  CustomWindow
//
//  Created by maulyn on 2018/5/28.
//  Copyright © 2018年 TGCASCE. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag{
    
    [NSApp.windows.firstObject makeKeyAndOrderFront:nil];
    return YES;
}

@end
